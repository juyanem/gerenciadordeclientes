#coding: utf-8
from django.db import models
from django import forms
from django.forms import ModelForm
from pedido.models import Pedido



class PedidoForms (ModelForm):
  
 
    class Meta:
        model = Pedido
        fields = ('titulo', 'descricao', 'prioridade', 'arquivo')
    
        