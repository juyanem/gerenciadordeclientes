#coding: utf-8

from django.db import models
from django import forms as forms
from django.contrib.auth.models import User
from django.forms.widgets import *

class Cliente (models.Model):
    empresa = models.CharField(max_length=100)
    telefone = models.CharField(max_length=50)
    email = models.EmailField()
    nomedoresponsavel = models.CharField(max_length=100, verbose_name="Nome do responsável")
    usuario = User()
    
    def __unicode__ (self):
        return self.nomedoresponsavel
    
class Pedido (models.Model):
    PRIORIDADE = (
                  ('ALTA','Alta'),
                  ('MEDIA','Media'),
                  ('BAIXA','Baixa'),
                  )
    cliente = models.ForeignKey(Cliente)
    titulo = models.CharField(max_length=100)
    descricao = models.TextField()
    prioridade = models.CharField(max_length=30, null=True,
                                  choices=PRIORIDADE)
    status = models.CharField(max_length=1, null=True,
                            choices=[['P','Pendente'], ['F', 'Finalizado']])
    datadopedido = models.DateField(auto_now_add=True)
    arquivo = models.FileField(upload_to='.')
    
    def __unicode__ (self):
        return self.titulo
       
    class Meta:
        db_table = "tbl_pedido"
