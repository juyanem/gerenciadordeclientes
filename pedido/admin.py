from django.contrib import admin
from pedido import models
from django.db.models.base import ModelBase

model_list = [m for m in dir(models) if isinstance(models.__getattribute__(m), ModelBase)]

for models_class in model_list:
    klass = models.__getattribute__(models_class)
    try:
        admin.site.register(klass)
    except:
        pass