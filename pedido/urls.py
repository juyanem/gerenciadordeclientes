from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'gerenciadordeclientes.views.home', name='home'),
    # url(r'^gerenciadordeclientes/', include('gerenciadordeclientes.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
      #url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
      url(r'^$', 'pedido.views.viewpedido'),
)
