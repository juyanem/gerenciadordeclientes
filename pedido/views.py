#coding: utf-8
# Create your views here.
from django.shortcuts import render_to_response, redirect
from pedido.models import Pedido
from django.template import RequestContext
from django.forms.widgets import *
from forms import PedidoForms

def viewpedido(request):
    
    if request.method == "POST":
        Pedido_forms = PedidoForms(request.POST, request.FILES)
        
        if Pedido_forms.is_valid():
            Pedido_forms.save()
            return redirect(viewpedido)
        else:
            return render_to_response("pedido/pedido.html", {"form":Pedido_forms}, context_instance= RequestContext(request)) 
    
    
    else:
        Pedido_forms = PedidoForms()
    
    
    return render_to_response ("pedido/pedido.html", {"form":Pedido_forms}, context_instance= RequestContext(request))
                                                          
    
                                                
        
    